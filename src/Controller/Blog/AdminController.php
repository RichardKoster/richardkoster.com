<?php

namespace App\Controller\Blog;

use App\Entity\Blog\Blog;
use App\Form\Blog\BlogType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class AdminController
 * @package App\Controller\Blog@
 *
 * @Route("/admin/blog")
 */
class AdminController extends Controller
{
    /**
     * @Route("/", name="blog_admin_index")
     *
     * @return Response
     */
    public function indexAction()
    {
        $blogs = $this->getDoctrine()->getRepository(Blog::class)->findAll();

        return $this->render(
            'bundles/Blog/Admin/index.html.twig',
            ['blogs' => $blogs]
        );
    }

    /**
     * @Route("/create", name="blog_admin_create")
     *
     * @param Request $request
     *
     * @return Response
     */
    public function createAction(Request $request): Response
    {
        $blog = new Blog();

        $form = $this->createForm(BlogType::class, $blog);
        $form->handleRequest($request);

        if ($form->isSubmitted()) {
            if ($form->isValid()) {
                $em = $this->getDoctrine()->getManager();
                $em->persist($blog);
                $em->flush();

                $this->addFlash('success', 'Blog was created');
                return $this->redirectToRoute('blog_admin_index');
            }

            $this->addFlash('danger', 'Blog could not be created');
        }

        return $this->render('bundles/Admin/form.html.twig', ['form' => $form->createView()]);
    }

    /**
     * @Route("/{id}/edit", name="blog_admin_edit")
     *
     * @param Request $request
     *
     * @param Blog    $blog
     *
     * @return Response
     */
    public function editAction(Request $request, Blog $blog): Response
    {
        $form = $this->createForm(BlogType::class, $blog);
        $form->handleRequest($request);

        if ($form->isSubmitted()) {
            if ($form->isValid()) {
                $em = $this->getDoctrine()->getManager();
                $em->persist($blog);
                $em->flush();

                $this->addFlash('success', 'Blog was edited');
                return $this->redirectToRoute('blog_admin_index');
            }

            $this->addFlash('danger', 'Blog could not be editted');
        }

        return $this->render('bundles/Admin/form.html.twig', [
            'form' => $form->createView(),
            'title' => 'Blog',
            'create' => $blog->getId() === null
        ]);
    }

    /**
     * @Route("/{id}/delete", name="blog_admin_delete")
     *
     * @param Blog $blog
     *
     * @return RedirectResponse
     */
    public function deleteAction(Blog $blog) {
        $em = $this->getDoctrine()->getManager();
        $em->remove($blog);
        $em->flush();

        $this->addFlash('success', 'Blog was removed');

        return $this->redirectToRoute('blog_admin_index');
    }
}