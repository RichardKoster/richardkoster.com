<?php

namespace App\Controller\Home;

use App\Entity\Blog\Blog;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Routing\Annotation\Route;

class HomeController extends Controller
{
    /**
     * @Route("/", name="home")
     */
    public function indexAction()
    {
        $blogs = $this->getDoctrine()->getRepository(Blog::class)->findAll();

        return $this->render('bundles/Home/index.html.twig', ['blogs' => $blogs]);
    }
}