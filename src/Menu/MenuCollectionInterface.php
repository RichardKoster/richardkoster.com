<?php

namespace App\Menu;

interface MenuCollectionInterface
{
    public function getMenuItems(): array;
}