<?php

namespace App\Menu;

interface MenuItemInterface
{
    /**
     * @return string
     */
    public function getName(): string;

    /**
     * @return string
     */
    public function getRoute(): string;
}