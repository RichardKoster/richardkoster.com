<?php

namespace App\Menu;

class BackendMenuItemCollection implements MenuCollectionInterface
{
    /**
     * @var array
     */
    protected $menuItems = [];

    /**
     * @param MenuItemInterface $menuItem
     */
    public function addMenuItem(MenuItemInterface $menuItem)
    {
        $this->menuItems[] = $menuItem;
    }

    /**
     * @return array
     */
    public function getMenuItems(): array
    {
        return $this->menuItems;
    }
}