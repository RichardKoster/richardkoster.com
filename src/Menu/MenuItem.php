<?php

namespace App\Menu;


class MenuItem implements MenuItemInterface
{
    /**
     * @var string
     */
    protected $name;

    /**
     * @var string
     */
    protected $route;

    public function __construct(string $name, string $route)
    {
        $this->name = $name;
        $this->route = $route;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @return string
     */
    public function getRoute(): string
    {
        return $this->route;
    }
}