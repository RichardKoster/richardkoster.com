<?php

namespace App\Menu;

class FrontendMenuItemCollection implements MenuCollectionInterface
{
    /**
     * @var array
     */
    protected $menuItems = [];

    /**
     * @param MenuItemInterface $menuItem
     */
    public function addMenuItem(MenuItemInterface $menuItem)
    {
        $this->menuItems[] = $menuItem;
    }

    /**
     * @return array
     */
    public function getMenuItems(): array
    {
        return $this->menuItems;
    }
}