<?php

namespace App\Menu;

use Knp\Menu\FactoryInterface;

class Builder
{
    /**
     * @var MenuCollectionInterface
     */
    private $backendMenuCollection;

    /**
     * @var FactoryInterface
     */
    private $factory;

    /**
     * @var MenuCollectionInterface
     */
    private $frontendMenuCollection;

    public function __construct(FactoryInterface $factory, MenuCollectionInterface $backendMenuCollection,
        MenuCollectionInterface $frontendMenuCollection)
    {
        $this->factory = $factory;
        $this->backendMenuCollection = $backendMenuCollection;
        $this->frontendMenuCollection = $frontendMenuCollection;
    }

    public function getBackendMenu(array $options)
    {
        $menu = $this->factory->createItem('root', ['currentClass' => 'active']);
        $menu->setChildrenAttribute('class', 'navbar-nav');

        foreach ($this->backendMenuCollection->getMenuItems() as $menuItem) {
            /** @var MenuItemInterface $menuItem */
            $menu->addChild($menuItem->getName(), ['route' => $menuItem->getRoute()]);
            $menu[$menuItem->getName()]->setAttribute('class', 'nav-item');
            $menu[$menuItem->getName()]->setLinkAttribute('class', 'nav-link');
        }

        return $menu;
    }

    public function getFrontendMenu(array $options)
    {
        $menu = $this->factory->createItem('root');
        $menu->setChildrenAttribute('class', 'navbar-nav');

        foreach ($this->frontendMenuCollection->getMenuItems() as $menuItem) {
            /** @var MenuItemInterface $menuItem */
            $menu->addChild($menuItem->getName(), ['route' => $menuItem->getRoute()]);
            $menu[$menuItem->getName()]->setAttribute('class', 'nav-item');
            $menu[$menuItem->getName()]->setLinkAttribute('class', 'nav-link');
        }

        return $menu;
    }
}