<?php

namespace App\DependencyInjection;

use Symfony\Component\DependencyInjection\Compiler\CompilerPassInterface;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Reference;

class MenuCompilerPass implements CompilerPassInterface
{
    /**
     * @param ContainerBuilder $container
     */
    public function process(ContainerBuilder $container)
    {
        $backendCollectionDefinition = $container->getDefinition('backend_menu_item_collection');
        foreach ($container->findTaggedServiceIds('backend_menu_item') as $id => $tags) {
            $backendCollectionDefinition->addMethodCall('addMenuItem', [new Reference($id)]);
        }
        $backendCollectionDefinition = $container->getDefinition('frontend_menu_item_collection');
        foreach ($container->findTaggedServiceIds('frontend_menu_item') as $id => $tags) {
            $backendCollectionDefinition->addMethodCall('addMenuItem', [new Reference($id)]);
        }
    }
}